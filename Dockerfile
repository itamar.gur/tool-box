FROM ubuntu:20.04

SHELL ["/bin/bash", "-c"]
RUN apt-get update
RUN export DEBIAN_FRONTEND=noninteractive && apt-get install -y -q apt-transport-https ca-certificates gnupg software-properties-common curl git unzip jq python3 python3-pip

RUN curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list

RUN apt-get update && apt-get install -y kubectl

RUN curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list

RUN apt-get update && apt-get install -y terraform

ADD https://github.com/gruntwork-io/terragrunt/releases/download/v0.36.3/terragrunt_linux_amd64 /usr/local/bin/terragrunt
RUN chmod +x /usr/local/bin/terragrunt

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

RUN apt-get install mysql-client -y

RUN echo "export PS1='\W->'" >> ~/.bashrc

# COPY bin/docker-init.sh /bin/docker-init.sh

# ENTRYPOINT [ "/bin/bash", "-c", "/bin/docker-init.sh" ]